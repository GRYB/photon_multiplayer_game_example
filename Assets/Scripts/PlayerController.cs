﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController: MonoBehaviourPunCallbacks, IPunObservable
{
    [SerializeField] public PhotonView pv;
    [SerializeField] float moveSpeed = 3f;
    [SerializeField] Sprite otherPlayerSprite;
    [SerializeField] GameObject ladderTilePrefab;
    [SerializeField] Transform ladder;

    public SpriteRenderer spriteRenderer;
    public Vector2Int gamePosition;
    public Vector2Int direction;
    MapController mapController;
    public bool isAlive = true;

    public Action PlayerKilled;
    void Awake()
    {
        mapController = GameManager.Instance.mapController;
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    void Start() {
        gamePosition = new Vector2Int((int)transform.position.x, (int)transform.position.y);
        mapController.AddPlayer(this);
        PhotonPeer.RegisterType(typeof(Vector2Int), 123, SerializeHelper.SerializeVector2Int, SerializeHelper.DeserializeVector2Int);
        SetSprite();
    }
    void Update()
    {
        if (photonView.IsMine && isAlive)
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                direction = Vector2Int.left;
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                direction = Vector2Int.right;
            }

            if (Input.GetKey(KeyCode.UpArrow))
            {
                direction = Vector2Int.up;
            }

            if (Input.GetKey(KeyCode.DownArrow))
            {
                direction = Vector2Int.down;
            }

        }

        if (direction == Vector2Int.left)
        {
            spriteRenderer.flipX = false;
        }

        if (direction == Vector2Int.right)
        {
            spriteRenderer.flipX = true;
        }

        //fps dependent, replace with animation
        transform.position = Vector3.Lerp(transform.position, (Vector2)gamePosition, Time.deltaTime * moveSpeed);

    }

    void SetSprite()
    {
        if (!photonView.IsMine)
        {
            spriteRenderer.sprite = otherPlayerSprite;
        }
    }

    public void SetLadderLength(int length)
    {
        for(int i = 0;  i < ladder.childCount; i++)
        {
            ladder.GetChild(i).gameObject.SetActive(i < length);
        }
        while(ladder.childCount < length)
        {
            Transform lastTilePosition = ladder.GetChild(ladder.childCount - 1);
            Instantiate(ladderTilePrefab, lastTilePosition.position + Vector3.down, Quaternion.identity, ladder);
        }
    }
    public void Die()
    {
        isAlive = false;
        if (PlayerKilled != null)
        {
            PlayerKilled.Invoke();
        }

    }

    void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        SceneManager.LoadScene(0);
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Debug.Log($"player {otherPlayer.NickName} left the room");
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(direction);
        }

        if (stream.IsReading)
        {
            direction = (Vector2Int) stream.ReceiveNext();
        }
    }

 
}

