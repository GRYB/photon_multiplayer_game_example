﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System;
using System.Linq;
using UnityEngine;


    public class GameTime : MonoBehaviour
    {
    [SerializeField] MapController mapController;
        void OnEnable()
        {
        PhotonPeer.RegisterType(typeof(Vector2Int), 123, SerializeHelper.SerializeVector2Int, SerializeHelper.DeserializeVector2Int);
    }
    float lastTimeTick;
        public static Action Tick; 
       

        private void Update()
        {
            if(PhotonNetwork.Time > lastTimeTick + 1 && PhotonNetwork.IsMasterClient 
                && PhotonNetwork.PlayerList.Length >= 2)
            {
            if (Tick != null)
            {
                Tick();

                var data = mapController.playerList.OrderBy(p => p.photonView.Owner.ActorNumber).
    Select(p => p.direction).ToArray();
                RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All};
                SendOptions sendOptions = new SendOptions { Reliability = true };
                PhotonNetwork.RaiseEvent(1, data, raiseEventOptions, sendOptions);
            }
 
                lastTimeTick = (float)PhotonNetwork.Time;
            }
        }

    }
