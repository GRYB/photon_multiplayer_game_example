﻿using ExitGames.Client.Photon;
using System;
using System.Collections;
using UnityEngine;

    public static class SerializeHelper
    {     
        public static object DeserializeVector2Int(byte [] data)
        {
            Vector2Int vector = new Vector2Int();
            vector.x = BitConverter.ToInt32(data, 0);
            vector.y = BitConverter.ToInt32(data, 4);
            return vector;

        }

        public static byte[] SerializeVector2Int(object obj)
        {
            Vector2Int vector2Int = (Vector2Int) obj;
            byte[] data = new byte[8];
            BitConverter.GetBytes(vector2Int.x).CopyTo(data, 0);
            BitConverter.GetBytes(vector2Int.y).CopyTo(data, 4);
            return data;
        }
    }
