﻿using UnityEngine;
public class SingletonMB<T> : MonoBehaviour where T : class
{
	static T instance;

	public static T Instance
	{
		get
		{
			if (instance == null)
			{
				Debug.Log("Instance not Inited");
			}
			return instance;
		}
	}

	public virtual void Awake()
	{
		instance = this as T;
	}
}
