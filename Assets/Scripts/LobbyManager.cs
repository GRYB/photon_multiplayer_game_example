﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using UnityEngine;

public class LobbyManager : MonoBehaviourPunCallbacks
{
    [SerializeField] string gameVersion = "1";

    void Start()
    {
        PhotonNetwork.NickName = "User" + Random.Range(1000,9999);
        PhotonNetwork.GameVersion = gameVersion;
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnected()
    {
        Debug.Log("OnConnected was called by PUN");
    }
    public override void OnConnectedToMaster()
    {
        Debug.Log("OnConnectedToMaster was called by PUN");
    }
    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.LogWarningFormat("OnDisconnected() was called by PUN with reason {0}", cause);
    }

    public void CreateRoom()
    {
        PhotonNetwork.CreateRoom(null);
    }
    public override void OnCreatedRoom()
    {
        Debug.Log("Room created");
    }
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Room  wasn't created, message: " + message);
    }

    public void JoinRandomRoom()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log(message);
    }
    public override void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom, Now this client is in a room.");
       // PhotonNetwork.LoadLevel("Level_1");
        StartCoroutine(LoadLevel("Level_1"));
    }

    IEnumerator LoadLevel(string levelName)
    {
        PhotonNetwork.LoadLevel(levelName);
        yield return null;
    }


    public void JoinPrivateRoom(string roomName)
    {
        PhotonNetwork.JoinRoom(roomName);
    }


}
