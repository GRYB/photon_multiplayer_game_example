﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MapController : MonoBehaviourPunCallbacks, IOnEventCallback
{
    [SerializeField] int gridX = 20;
    [SerializeField] int gridY = 10;
    [SerializeField] GameObject cellPrefab;
    GameObject[,] cells;
    public List<PlayerController> playerList = new List<PlayerController>();
    public override void OnEnable()
    {
        base.OnEnable();
        PhotonNetwork.AddCallbackTarget(this);
        GameTime.Tick += OnTick;
    }
    void Start()
    {
        cells = new GameObject[gridX, gridY];
        GenerateMap();
    }

    public void AddPlayer(PlayerController player)
    {
        playerList.Add(player);
        cells[player.gamePosition.x, player.gamePosition.y].SetActive(false);
    }

    void GenerateMap()
    {
        for (int x = 0; x < gridX; x++)
        {
            for (int y = 0; y < gridY; y++)
            {
                cells[x, y] = Instantiate(cellPrefab, new Vector2(x, y), Quaternion.identity, transform);
            }
        }
    }

    public void OnEvent(EventData photonEvent)
    {
        switch (photonEvent.Code)
        {
            case 1:
                {
                    directions = (Vector2Int[])photonEvent.CustomData;
                    MovePlayers(directions);
                    break;
                }
        }
    }

    void OnTick()
    {

    }

    Vector2Int[] directions;
    void MovePlayers(Vector2Int[] directions)
    {
        PlayerController[] sortedPlayers = playerList.Where(p => p.isAlive).OrderBy(p => p.photonView.Owner.ActorNumber).
            ToArray();
        if (playerList.Count != directions.Length)
        {
            Debug.LogWarning("playerList.Count != directions.Length");
            return;
        }

        int i = 0;
        foreach (var player in sortedPlayers)
        {
            player.direction = directions[i++];
            MinePlayerBlock(player);
        }

        foreach (var player in sortedPlayers)
        {
            MovePlayer(player);
        }
    }

    
    void MinePlayerBlock(PlayerController player)
    {
        Vector2Int targetPosition = player.gamePosition + player.direction;

        if (targetPosition.x < 0) return;
        if (targetPosition.y < 0) return;
        if (targetPosition.x >= gridX) return;
        if (targetPosition.y >= gridY) return;

        cells[targetPosition.x, targetPosition.y].SetActive(false);

        CheckPlayerKilled(targetPosition, player);
    }

    private void CheckPlayerKilled(Vector2Int position, PlayerController player)
    {
        PlayerController myPlayer = playerList.First(p => p.photonView.IsMine);
        if(myPlayer != player)
        {
            while(!cells[position.x, position.y].activeSelf && position.y < gridY - 1)
            {
                if(position == myPlayer.gamePosition)
                {
                    myPlayer.Die();
                    break;
                }
                position.y++;
            }
        }
    }
    
    void MovePlayer(PlayerController player){

        player.gamePosition += player.direction;
        if (player.gamePosition.x < 0) player.gamePosition.x = 0;
        if (player.gamePosition.y < 0) player.gamePosition.y = 0;
        if (player.gamePosition.x >= gridX) player.gamePosition.x = gridX - 1;
        if (player.gamePosition.y >= gridY) player.gamePosition.y = gridY - 1;

        int ladderLength = 0;
        Vector2Int pos = player.gamePosition;
        while (pos.y > 0 && !cells[pos.x, pos.y - 1].activeSelf)
        {
            ladderLength++;
            pos.y--;
        }
        player.SetLadderLength(ladderLength);
    }

    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.RemoveCallbackTarget(this);
        GameTime.Tick -= OnTick;
    }
}
