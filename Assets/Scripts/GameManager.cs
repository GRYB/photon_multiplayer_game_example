﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : SingletonMB<GameManager>
    //MonoBehaviourPunCallbacks
{
    [SerializeField] GameObject playerPrefab;
    [SerializeField] public MapController mapController;
    
    void Start()
    {
        PhotonNetwork.Instantiate(playerPrefab.name,new Vector2(Random.Range(1,20), Random.Range(1,10)), Quaternion.identity);
    }

    public void LeaveRoom() {
        PhotonNetwork.LeaveRoom();
    }
}
